require 'rails_helper'

RSpec.describe "Categories", type: :request do
  let(:taxon) { create(:taxon) }
  let(:taxonomy) { create(:taxonomy) }

  before do
    get potepan_category_path(taxon.id)
  end

  it 'showテンプレートが表示されていること' do
    expect(response).to render_template(:show)
  end

  it '正常なレスポンスを返すこと' do
    expect(response).to have_http_status(200)
  end

  it "taxon名を取得している" do
    expect(response.body).to include taxon.name
  end

  it "taxonomy名を取得している" do
    expect(response.body).to include taxonomy.name
  end
end
