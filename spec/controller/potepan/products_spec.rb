require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    subject { get :show, params: { id: product.id } }

    let(:product) { create(:product) }

    it "レスポンスの成功確認" do
      expect(response).to be_successful
    end

    it 'テンプレートがレンダリングされること' do
      subject
      expect(response).to render_template :show
    end

    it "@productが期待される値を持つ" do
      subject
      expect(assigns(:product)).to eq(product)
    end
  end
end
