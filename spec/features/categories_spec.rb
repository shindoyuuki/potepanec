require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon) { create(:taxon, name: "Bags", parent: taxonomy.root, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, name: "Mags", parent: taxonomy.root, taxonomy: taxonomy) }
  let!(:product) { create(:product, name: "bag", taxons: [taxon]) }
  let!(:other_product) { create(:product, name: "mag", taxons: [other_taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it "カテゴリーページが正しく表示されていること" do
    expect(page).to have_link taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_selector('img[alt="products-img"]')
    expect(page).not_to have_content other_product.name
    within ".side-nav" do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
    end
  end

  it "taxon.products.countがtaxonと紐付くproduct数と合っているか" do
    expect(page).to have_content "(#{taxon.products.count})"
    expect(page).to have_content "(1)"
  end

  it "サイドバーのカテゴリーからカテゴリーページへ遷移すること" do
    click_on taxonomy.name
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  it "商品カテゴリから商品詳細ページへの移動を確認すること" do
    expect(page).to have_link product.name
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
