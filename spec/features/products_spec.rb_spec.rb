require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.feature "Products", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "category") }
  let(:taxon) { create(:taxon, name: 'bags', parent: taxonomy.root, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, name: 'mags', parent: taxonomy.root, taxonomy: taxonomy) }
  let(:product) { create(:product, name: "BAG", price: "20.00", taxons: [taxon]) }

  scenario '商品詳細ページにアクセス' do
    visit potepan_product_path(product.id)
    expect(page).to have_current_path potepan_product_path(product.id)
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
    expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(taxon.id)
  end
end
