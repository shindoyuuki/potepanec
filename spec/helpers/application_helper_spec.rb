require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    context 'page_titleが空の場合' do
      it 'BASE_TITLEが表示される' do
        expect(full_title('')).to eq 'BIGBAG Store'
      end
    end

    context 'page_titleがnilの場合' do
      it 'BASE_TITLEが表示される' do
        expect(full_title(nil)).to eq 'BIGBAG Store'
      end
    end

    context 'page_titleが空ではない場合' do
      it '(page_title | BASE_TITLE)が表示される' do
        expect(full_title('page_title')).to eq('page_title | BIGBAG Store')
      end
    end
  end
end
